package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;


@SpringBootApplication
//This application will function as an endpoint that will be used in handling http request.
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	ctrl + c to stop the server and application

	//	localhost:8080/hello
	@GetMapping("/hello")
//	Maps a get request to the route "/hello" and invokes the method hello().
	public String hello() {
		return "Hello World";
	}

	//	Route with String Query
//	localhost:8080/hi?name=value
	@GetMapping("/hi")
//	"@RequestParam" annotation that allows us to extract data from query strings in the URL
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	//	Route with path variables
//	Dynamic data is obtained directly from the url
//	localhost:8080/name
	@GetMapping("/hello/{name}")
//	"@PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}

	// Activity s09 intro to spring boot

    // 1. Create an arraylist called enrollees
    private ArrayList<String> enrollee = new ArrayList<>();

    //2. Create a //enroll route that will accept a query string with parameter of user
    //localhost:8080/greeting/enroll?user=value
    @GetMapping("/enroll")
    public String enroll(@RequestParam(value="user", defaultValue = "Lambojo") String user){
        enrollee.add(user);
        return String.format("Thank you for enrolling, %s!", user);
    }


    //3. Create a new /getEnrollees route which will return the content of the enrollees Arraylist as a string
    //localhost:8080/greeting/getEnrollees
    @GetMapping("/getEnrollees")
    public String getEnrollees(){
        return enrollee.toString();
    }

    //4. Create a /nameage route that will accept multiple query string parameters of name and age
    //localhost:8080/greeting/nameage?name=value&age=value
    @GetMapping("/nameage")
    public String nameage(@RequestParam(value="name", defaultValue = "Lambojo") String name, @RequestParam(value = "age", defaultValue = "21") String age){
        return String.format("Hello %s! My age is %s.", name, age);
    }

    //5. Create a /course/id dynamic route using a path variable of id
    //localhost:8080/greeting/courses/value
    @GetMapping("/course/{id}")
    public String courses(@PathVariable("id") String id){
        String name, sched, price;

        switch (id) {
            case "java101":
                name = "Java 101";
                sched = "MWF 8:00AM-11:00PM";
                price = "PHP 3000";
                return String.format("Name: " + name +", " + "Schedule: "+ sched +", "+ "Price: " + price);
            case "sql101":
                name = "SQL 101";
                sched = "TTH 1:00PM-4:00PM";
                price = "PHP 2000";
                return String.format("Name: " + name +", " + "Schedule: "+ sched +", "+ "Price: " + price);
            case "javaee101":
                name = "Java EE 101";
                sched = "MWF 1:00PM-4:00PM";
                price = "PHP 3500";
                return String.format("Name: " + name +", " + "Schedule: "+ sched +", "+ "Price: " + price);
            default:
                return String.format("Course cannot be found.");
        }

    }

	//	Activity for s09 Asynchronous
// 	localhost:8080/welcome?/user=value&role=value
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value = "user", defaultValue = "Joe") String user, @RequestParam(value = "role", defaultValue = "Teacher") String role) {
		switch (role) {
			case "Admin":
				return String.format("Welcome back to the class portal,Admin %s!", user);
			case "Teacher":
				return String.format("Thank you for logging in, Teacher %s!", user);
			case "Student":
				return String.format("Welcome back to the class portal, %s!", user);
			default:
				return String.format("Role out of range!");

		}
	}

	//locallhost:8080/greeting/register?id=123456789&name=John&course=BSIT
	private class Student {
		private String name;
		private String id;
		private String course;

		public Student(String name, String id, String course) {
			this.name = name;
			this.id = id;
			this.course = course;
		}

		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public String getCourse() {
			return course;
		}
	}

	private ArrayList<Student> students = new ArrayList<>();

	@GetMapping("/register")
	public String register(@RequestParam(value = "id", defaultValue = "123456789") String id, @RequestParam(value = "name", defaultValue = "Jane") String name, @RequestParam(value = "course", defaultValue = "BSIT") String course) {
		Student student = new Student(name, id, course);
		students.add(student);
		return String.format("%s your ID number is registered on the system!", id);
	}


	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return "Welcome back " + student.getName() + "! You are currently enrolled in " + student.getCourse() + ".";
			}
		}
		return "Your provided " + id + " is not found on the system!";

	}
}